//1
var myinfo = (film: "Home alone", favoriteNumber: 30, dish: "Barbecue")
//2
let (fil , num, dis) = myinfo
print("const:" + fil , num, dis)
//3
var friendInfo = (film: "Brave heart", favoriteNumber: 11, dish: "coffe")
//4
print("myinfo:     \(myinfo)")
print("friendInfo: \(friendInfo)")
var optionalTuple = (myinfo.film, myinfo.favoriteNumber, myinfo.dish)
myinfo = friendInfo
friendInfo = optionalTuple
print("newMyinfo:     \(myinfo)")
print("newFriendInfo: \(friendInfo)")
//5
let newTuple = (myinfo.favoriteNumber, friendInfo.favoriteNumber, myinfo.favoriteNumber - friendInfo.favoriteNumber)
print (newTuple)

let (a, b, c) = myinfo
let (d, e, f) = friendInfo
myinfo = (d,e,f)
friendInfo = (a, b, c)
print (myinfo)
print (friendInfo)

func checkCell ( entryTuple:(row: Int, column: Int) ) -> String {
    var result = ""
    if (entryTuple.row >= 1) && (entryTuple.column >= 1) {
        if (entryTuple.row + entryTuple.column) % 2 == 0 {
            result = "Cell is black"
            } else {
            result =  "Cell is white"
            }
    } else {
    result = "***Wrong inputed arguments***"
    }
return result 
}

print(checkCell(entryTuple :(row: 0, column: 6)))

